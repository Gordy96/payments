<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/02/2018
 * Time: 10:33
 */

namespace Johnny\Payments;

interface ApiTokenRepository
{
    /**
     * @return string
     */
    public function GetToken();

    /**
     * @param mixed ...$args
     * @return mixed
     */
    public function SetToken(...$args);
}