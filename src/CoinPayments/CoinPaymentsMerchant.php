<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 01/03/2018
 * Time: 10:57
 */

namespace Johnny\Payments\CoinPayments;

use Johnny\Payments\PaymentApiInterface;
use Johnny\Payments\PaymentRepository;
use Johnny\Payments\ResponseBuilderInterface;

class CoinPaymentsMerchant implements PaymentApiInterface
{

    private $merchant_id;
    private $secret;

    /**
     * @var PaymentRepository
     */
    private $payments;

    /**
     * @var ResponseBuilderInterface
     */
    private $response;

    /**
     * @inheritDoc
     */
    public function CreatePayment($sum, ...$args)
    {
        return $this->render($sum, ...$args);
    }

    /**
     * @inheritDoc
     */
    public function ConfirmPayment($request)
    {
        if (!isset($_SERVER['HTTP_HMAC']) || empty($_SERVER['HTTP_HMAC'])) {
            throw new \Exception("No HMAC signature sent");
        }

        $merchant = isset($request['merchant']) ? $request['merchant']:'';
        if (empty($merchant)) {
            throw new \Exception("No Merchant ID passed");
        }

        if ($merchant != $this->merchant_id) {
            throw new \Exception("Invalid Merchant ID");
        }

        $rs = file_get_contents('php://input');
        if ($rs === FALSE || empty($rs)) {
            throw new \Exception("Error reading POST data");
        }

        $hmac = hash_hmac("sha512", $rs, $this->secret);
        if ($hmac != $_SERVER['HTTP_HMAC']) {
            throw new \Exception("HMAC signature does not match");
        }
        if($request['status'] >= 100){
            $payment = $this->payments->FindPayment($request['invoice']);
            $this->payments->UpdatePaymentInfo(["txn_id"=>$request['txn_id']]);
            return $this->payments->ConfirmationSucceed($request['received_amount']);
        }
        return $this->payments->ConfirmationFailed();
    }

    public function GetSucceedResponse()
    {
        return $this->response->Success();
    }

    public function GetFailedResponse()
    {
        return $this->response->Failed();
    }

    /**
     * @inheritDoc
     */
    public function __construct(PaymentRepository $payment_repository, ResponseBuilderInterface $responser, array $config = null)
    {
        $this->merchant_id = $config['merchant_id'];
        $this->secret = $config['secret'];
        $this->payments = $payment_repository;
        $this->response = $responser;
    }

    private function render($sum, ...$args){
        return "<form action=\"https://www.coinpayments.net/index.php\" method=\"post\">
	<input type=\"hidden\" name=\"cmd\" value=\"_pay_simple\">
	<input type=\"hidden\" name=\"reset\" value=\"1\">
	<input type=\"hidden\" name=\"merchant\" value=\"{$this->merchant_id}\">
	<input type=\"hidden\" name=\"currency\" value=\"{$args[0]}\">
	<input type=\"hidden\" name=\"amountf\" value=\"{$sum}\">
	<input type=\"hidden\" name=\"item_name\" value=\"{$args[2]}\">	
	<input type=\"hidden\" name=\"invoice\" value=\"{$args[1]}\">	
	<input type=\"hidden\" name=\"success_url\" value=\"{$args[3]}\">	
	<input type=\"hidden\" name=\"cancel_url\" value=\"{$args[4]}\">	
	<input type=\"submit\" value='buy'>
</form>";
    }

}