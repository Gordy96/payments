<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/02/2018
 * Time: 12:37
 */

namespace Johnny\Payments;


interface PaymentRepository
{
    /**
     * @param mixed $credentials
     * @throws PaymentNotFoundException
     * @return mixed
     */
    public function FindPayment($credentials);

    public function UpdatePaymentTx($credentials);

    public function UpdatePaymentInfo($credentials);

    public function ConfirmationSucceed($price);

    public function ConfirmationFailed();
}