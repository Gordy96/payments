<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 08.03.18
 * Time: 11:22
 */

namespace Johnny\Payments;


interface ResponseBuilderInterface
{
    public function Success();

    public function Failed();
}