<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 26/02/2018
 * Time: 14:14
 */

namespace Johnny\Payments;

interface PaymentApiInterface
{
    /**
     * @param float $sum
     * @param mixed $args additional arguments
     * @return mixed
     */
    public function CreatePayment($sum, ...$args);

    /**
     * @param array|mixed $request request parameters bag
     * @throws \Exception
     * @return mixed
     */
    public function ConfirmPayment($request);

    /**
     * PaymentApiInterface constructor.
     * @param PaymentRepository $payment_repository
     * @param ResponseBuilderInterface $responser
     * @param array $config
     */
    public function __construct(PaymentRepository $payment_repository, ResponseBuilderInterface $responser, array $config = null);

    public function GetSucceedResponse();

    public function GetFailedResponse();
}