<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/02/2018
 * Time: 10:32
 */

namespace Johnny\Payments\PayPal;

use Johnny\Payments\ApiTokenRepository;

class PayPalTokenRepository implements ApiTokenRepository
{
    /**
     * @inheritDoc
     */
    public function GetToken()
    {
        $temp = json_decode(file_get_contents(__DIR__ . '/key.json'));
        if($temp){
            if($temp->expires_at < time() - 100)
                return $temp->access_token;
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function SetToken(...$args)
    {
        file_put_contents(__DIR__ . '/key.json', json_encode(['access_token'=>$args[0],'expires_at'=>$args[1]]));
    }

}