<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/02/2018
 * Time: 11:27
 */

namespace Johnny\Payments\PayPal\Objects;


class Payer
{
    public $payment_method = 'paypal';
    public function __construct($method = null)
    {
        if($method)
            $this->payment_method = $method;
    }
}