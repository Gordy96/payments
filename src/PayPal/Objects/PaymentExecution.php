<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/02/2018
 * Time: 14:44
 */

namespace Johnny\Payments\PayPal\Objects;


class PaymentExecution
{
    public $payer_id;

    public function __construct($id)
    {
        $this->payer_id = $id;
    }
}