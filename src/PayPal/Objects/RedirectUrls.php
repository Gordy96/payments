<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/02/2018
 * Time: 11:29
 */

namespace Johnny\Payments\PayPal\Objects;


class RedirectUrls
{
    public $return_url;

    public $cancel_url;

    public function __construct($return_url, $cancel_url)
    {
        $this->return_url = $return_url;
        $this->cancel_url = $cancel_url;
    }
}