<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/02/2018
 * Time: 11:13
 */

namespace Johnny\Payments\PayPal\Objects;


class Payment
{
    public $intent = 'sale';
    public $redirect_urls;
    public $payer;
    public $transactions;
    public function __construct($sum, $currency, $return_url, $cancel_url)
    {
        $this->redirect_urls = new RedirectUrls($return_url, $cancel_url);
        $this->payer = new Payer();
        $this->transactions = [
            new Transaction($sum, strtoupper($currency))
        ];
    }
}