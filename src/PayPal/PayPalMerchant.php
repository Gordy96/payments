<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 27/02/2018
 * Time: 16:43
 */

namespace Johnny\Payments\PayPal;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Johnny\Payments\PaymentApiInterface;
use Johnny\Payments\ApiTokenRepository;
use GuzzleHttp\Client;
use Johnny\Payments\PaymentRepository;
use Johnny\Payments\PayPal\Objects\Payment;
use Johnny\Payments\PayPal\Objects\PaymentExecution;
use Johnny\Payments\ResponseBuilderInterface;

class PayPalMerchant implements PaymentApiInterface
{

    /**
     * @var string
     */
    protected $client_id;

    /**
     * @var string
     */
    protected $client_secret;

    /**
     * @var \GuzzleHttp\Client
     */
    private $http;

    /**
     * @var string
     */
    private $token = '';

    /**
     * @var ApiTokenRepository
     */
    private $repo;

    public function GetSucceedResponse()
    {
        return $this->response->Success();
    }

    public function GetFailedResponse()
    {
        return $this->response->Failed();
    }

    /**
     * @var PaymentRepository
     */
    private $payments;

    /**
     * @var ResponseBuilderInterface
     */
    private $response;

    public function __construct(PaymentRepository $payment_repository, ResponseBuilderInterface $responser, array $config = null)
    {
        $this->client_id = $config['client_id'];
        $this->client_secret = $config['client_secret'];
        $this->payments = $payment_repository;
        $this->response = $responser;
        $this->http = new Client([
            'base_uri'=>'https://api.sandbox.paypal.com/v1/',
            'verify' => false
        ]);
    }

    /**
     * @param ApiTokenRepository $repository
     */
    public function SetTokenRepository($repository){
        $this->repo = $repository;
    }

    public function RetrieveToken(){
        $this->token = $this->repo->GetToken();
        if($this->token == null){
            $temp = $this->auth();
            $this->token = $temp->access_token;
            $this->repo->SetToken($temp->access_token, time() + $temp->expires_in - 100);
        }
        return $this->token;
    }

    /**
     * @return mixed
     */

    private function auth(){
        $res = $this->http->post('oauth2/token', [
            'form_params'=>['grant_type'=>'client_credentials'],
            'auth'=>[$this->client_id, $this->client_secret],
            'headers'=>['accept'=>'application/json']
        ])->getBody()->getContents();
        return json_decode($res);
    }

    /**
     *
     */

    private function send(...$params){
        $prefs = [
            'headers'=>[
                'authorization'=>"Bearer {$this->RetrieveToken()}",
                'accept'=>'application/json'
            ]
        ];
        if(isset($params[1])){
            switch(gettype($params[1])){
                case 'string':
                    $prefs['query'] = $params[1];
                    break;
                case 'array':
                default:
                    $prefs['json'] = $params[1];
            }
        }

        if(isset($params[2])){
            $prefs['headers'] = array_merge($prefs['headers'], $params[2]);
        }

        $res = $this->http->post($params[0],$prefs)->getBody()->getContents();
        return json_decode($res);
    }

    /**
     * @inheritDoc
     */
    public function CreatePayment($sum, ...$args)
    {
        $res = $this->send("payments/payment", new Payment($sum, $args[0], $args[1], $args[2]));
        $this->payments->UpdatePaymentTx($res->id);
        return $res;
    }

    /**
     * @inheritDoc
     */
    public function ConfirmPayment($request)
    {
        if(isset($request['paymentId']) && isset($request['PayerID'])){
            $payment = $this->payments->FindPayment($request['paymentId']);
            $res = $this->send("payments/payment/{$request['paymentId']}/execute", new PaymentExecution($request['PayerID']));
            if($res->state === "approved"){
                return $this->payments->ConfirmationSucceed($res->transactions[0]->amount->total);
            } else {
                return $this->payments->ConfirmationFailed();
            }
        }
        return null;
    }
}