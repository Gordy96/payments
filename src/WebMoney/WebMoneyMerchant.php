<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 26/02/2018
 * Time: 14:52
 */

namespace Johnny\Payments\WebMoney;

use Johnny\Payments\PaymentApiInterface;
use Johnny\Payments\PaymentRepository;
use Johnny\Payments\ResponseBuilderInterface;

class WebMoneyMerchant implements PaymentApiInterface
{
    private $purse;

    /**
     * @var string Secret key from WM
     */
    private $secret;

    /**
     * @var array sign formation request fields order
     */

    private static $sign_fields = [
        'LMI_PAYEE_PURSE',
        'LMI_PAYMENT_AMOUNT',
        'LMI_PAYMENT_NO',
        //'LMI_MODE', //Disable if not in test mode
        'LMI_SYS_INVS_NO',
        'LMI_SYS_TRANS_NO',
        'LMI_SYS_TRANS_DATE',
        'LMI_SECRET_KEY',
        'LMI_PAYER_PURSE',
        'LMI_PAYER_WM'
    ];


    /**
     * @var PaymentRepository
     */
    private $payments;

    /**
     * @var ResponseBuilderInterface
     */
    private $response;

    /**
     * @inheritdoc
     */

    public function __construct(PaymentRepository $repo, ResponseBuilderInterface $responser, array $config = null)
    {
        $this->purse = $config['purse'];
        $this->secret = $config['secret'];
        $this->payments = $repo;
        $this->response = $responser;
    }

    public function GetSucceedResponse()
    {
        return $this->response->Success();
    }

    public function GetFailedResponse()
    {
        return $this->response->Failed();
    }


    /**
     * @inheritdoc
     */
    public function CreatePayment($sum, ...$args)
    {
        return $this->render($sum, ...$args);
    }

    /**
     * @inheritdoc
     */
    public function ConfirmPayment($request)
    {
        $payment = $this->payments->FindPayment($request['LMI_PAYMENT_NO']);
        if(isset($request['LMI_PREREQUEST']) && $payment->value <= $request['LMI_PAYMENT_AMOUNT'])
            return 'YES';
        if($this->check_sign($request)){
            $this->payments->UpdatePaymentInfo([
                'wm_transaction_id'=>$request['LMI_SYS_TRANS_NO'],
                'wm_from_wallet'=>$request['LMI_PAYER_PURSE'],
                'wm_from_id'=>$request['LMI_PAYER_WM']
            ]);
            return $this->payments->ConfirmationSucceed($request['LMI_PAYMENT_AMOUNT']);
        } else {
            return $this->payments->ConfirmationFailed();
        }
    }

    /**
     * @param float $sum
     * @param mixed $args additional arguments
     * @return string
     */
    private function render($sum, ...$args)
    {
        $purse = $this->purse;
        return "<form method=\"POST\" action=\"https://merchant.webmoney.ru/lmi/payment_utf.asp\" accept-charset=\"utf-8\">
                    <input type=\"hidden\" name=\"LMI_PAYMENT_AMOUNT\" value=\"{$sum}\">
                    <input type=\"hidden\" name=\"LMI_PAYMENT_DESC\" value=\"{$args[1]}\">
                    <input type=\"hidden\" name=\"LMI_PAYMENT_NO\" value=\"{$args[0]}\">
                    <input type=\"hidden\" name=\"LMI_PAYEE_PURSE\" value=\"{$purse}\">
                    <input type=\"hidden\" name=\"LMI_SIM_MODE\" value=\"0\">
                    <input type=\"submit\">
                </form>";
    }

    /**
     * @param $request
     * @throws \ErrorException
     * @return bool
     */

    protected function check_sign($request){
        $sign = '';
        foreach (self::$sign_fields as $field){
            if($field === 'LMI_SECRET_KEY')
                $sign .= $this->secret;
            else{
                if(isset($request[$field]))
                    $sign .= $request[$field];
                else
                    throw new \ErrorException("Request corrupted");
            }
        }
        return strtoupper(hash('sha256', $sign)) === $request['LMI_HASH'];
    }

}